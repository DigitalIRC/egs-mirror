		<!-- 
			// Copyright
			// Please leave this alone, I'm allowing you to use EGs FREE, the leaset
			// you can do it leave this one copyright tag alone.
			// kthx - synmuffin
		-->
		<footer id="copyright"><a href="irc://irc.digitalirc.org/egs">EGs Web Panel</a> - (Copyright &copy; <a href="http://epicgeeks.net/egs/">synmuffin</a>)</footer>
        <!-- Piwik -->
        <script type="text/javascript">
          var _paq = _paq || [];
          _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
          _paq.push(["setCookieDomain", "*.digitalirc.org"]);
          _paq.push(['trackPageView']);
          _paq.push(['enableLinkTracking']);
          (function() {
            var u="//metrics.digitalirc.org/";
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', 2]);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
          })();
        </script>
        <noscript><p><img src="//metrics.digitalirc.org/piwik.php?idsite=2" style="border:0;" alt="" /></p></noscript>
        <!-- End Piwik Code -->
	</div>
</body>
</html>